
export default {

  TextInput: {
    color: '#FFFFFF',
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 3,
    marginLeft: 10,
    marginTop: 8
  },
  IconTextInput: {
    fontSize: 30
  },
  textFontSize: {
    fontSize: 16
  },
  lable: {
    color: '#FFFFFF',
  },
  icon_Arrow:
  {
    borderRadius: 55,
    width: 55,
    height: 60
  },
  tabStyle: { backgroundColor: "#FFFFFF" },
  textStyle: { color: "#D4D4D4" },
  activeTabStyle: { backgroundColor: '#FFFFFF' },
  activeTextStyle: { color: '#385EBD' },
  view_tab:
  {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginTop: 150
  },
  tab_text:
  {
    color: '#D1D6E5',
    fontSize: 34,
    fontWeight: 'bold',
  }

}
