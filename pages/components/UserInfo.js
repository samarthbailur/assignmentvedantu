import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Header, Item, Thumbnail, Input, Icon, Button, Tab, Tabs, Footer, FooterTab, Content, List, ListItem, Left, Body, Card, Right, ScrollableTab } from 'native-base';
import { Platform, StyleSheet, Text, View, TouchableOpacity, ToastAndroid, ScrollView, Image, FlatList, TouchableNativeFeedback, Linking } from 'react-native';
import Style from "./style";
import Book_Icon from 'react-native-vector-icons/AntDesign';
import Icon_star from 'react-native-vector-icons/FontAwesome';
import Icon_dot from 'react-native-vector-icons/Entypo';
class UserInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            git_data: [],
            Page : 0

        }

    }
    async componentDidMount() {
        this.get_data();
    }
    async get_data() {
        try {
            let response = await fetch(
                'https://api.github.com/users/supreetsingh247/repos',
            );
            let responseJson = await response.json();
            //  alert(JSON.stringify(responseJson));
            if (responseJson) {
                this.setState({
                    git_data: responseJson,
                    Page : 1
                })

            }
            // alert("hello"+JSON.stringify(this.state.git_data))

        } catch (error) {
            ToastAndroid.show('Something Went Wrong', ToastAndroid.SHORT);
        }
    }
    render() {
        return (
            <Container>
                
                <ScrollView >
                    <View >
                        <Tabs locked initialPage={this.state.Page} tabBarUnderlineStyle={{ backgroundColor: '#000' }}  >
                            <Tab tabStyle={Style.tabStyle} textStyle={Style.textStyle} activeTabStyle={Style.activeTabStyle} activeTextStyle={Style.activeTextStyle} heading="Overview">
                                <View  style={Style.view_tab}>
                                    <Text style={Style.tab_text}>Overview</Text>

                                </View>
                            </Tab>
                            <Tab tabStyle={Style.tabStyle} textStyle={Style.textStyle} activeTabStyle={Style.activeTabStyle} activeTextStyle={Style.activeTextStyle} heading="Repositories">
                                <View >
                                    <FlatList
                                        data={this.state.git_data}
                                        renderItem={({ item, index }) =>

                                            <View>
                                                <List key={index} style={{ marginTop: 4 }}>
                                                    <ListItem thumbnail>
                                                        <Left>
                                                            {/* <Thumbnail round source={{ uri: item.avatar }} /> */}
                                                            <Book_Icon name='book' size={25} color="#000" />
                                                        </Left>
                                                        <Body>
                                                            <Text style={{ marginBottom: 15, color: 'blue' }} onPress={() => Linking.openURL(item.html_url)}>{item.full_name}</Text>
                                                            <Text style={{ marginBottom: 15 }}>{item.description}</Text>

                                                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                                                <Text style={{ width: 50 }}><Icon_star name='star' size={15} color="#000" />{item.stargazers_count}</Text>
                                                                <Icon_dot style={{ position: 'absolute', marginTop: -20, marginLeft: 10 }} name='dot-single' size={60} color="#dae525" />
                                                                <Text style={{ width: 150 }}>{item.language}</Text>
                                                            </View>
                                                        </Body>
                                                        
                                                    </ListItem>
                                                </List>
                                            </View>
                                        }
                                    //  keyExtractor={item => item.id}
                                    //  ItemSeparatorComponent={this.renderSeparator}
                                    //  ListHeaderComponent={this.renderHeader} 
                                    />
                                </View>
                            </Tab>
                            <Tab tabStyle={Style.tabStyle} textStyle={Style.textStyle} activeTabStyle={Style.activeTabStyle} activeTextStyle={Style.activeTextStyle} heading="Stars">
                                <View style={Style.view_tab}>
                                    <Text style={Style.tab_text}>Stars</Text>
                                </View>
                            </Tab>
                            <Tab tabStyle={Style.tabStyle} textStyle={Style.textStyle} activeTabStyle={Style.activeTabStyle} activeTextStyle={Style.activeTextStyle} heading="Follow">
                                <View style={Style.view_tab}>
                                    <Text style={Style.tab_text}>Follow</Text>
                                </View>
                            </Tab>

                        </Tabs>





                    </View >
                </ScrollView>
            </Container >
        );
    }
}

UserInfo.propTypes = {

};

export default UserInfo;