import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import UserInfo from './pages/components/UserInfo';

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (

      <AppStack />

    );
  }
}
//your new text color
const myNewTextColor = 'forestgreen';

//your new header background color
const myNewHeaderBackgroundColor = 'pink';
const AppStack = createStackNavigator(
  {
    Home: {
      screen: UserInfo,
      navigationOptions: {
        headerStyle: {
          backgroundColor: '#000',

        },
        headerBackground: (
         
          <Image

            style={{ width: 40, height: 40, borderRadius: 25, marginLeft: 150 }}
            source={require('./pages/components/img/git_hub.png')}
          />
        ),
      }
    }

  },
  {
    initialRouteName: 'Home',
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
